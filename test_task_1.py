"""
Тестовое задание, которое размещено на https://myte.me/tasks/nvi7JpEcZbFVf4bbZCQ0
--------------------------------------------------------------
1.Есть список input_list (python list) целых чисел длинной N. Необходимо выбрать k случайных элементов без повторений по индексу .
Алгоритм решения должен быть оптимальным по сложности и по ресурсам, определить сложность в нотации O(N,k)

Переводить список в другие структуры данных нельзя. Для получения единичного случайного числа можно использовать метод random.randint(A, B).
Иные методы из random и в целом сторонние библиотеки использовать нельзя

PS. Пожелание: не забудьте рассмотреть случай k —> N

Пример:

input_list = [1, 3, 4, 3, 3, 9]

k = 3

варианты ответа: [3, 4, 3] или [4, 1, 9] или [3, 1, 4]
неправильные вариант ответа: [4, 4, 3] - 4 не должен быть более одного раза, потому что во входном списке встречается только единожды

2.Дополнительно: Решить предыдущую задачу с условием того, что для каждого элемента входного списка есть соответствующая вероятность его выпадения.
Вероятности представлены отдельным список той же длины.
Пример списка вероятностей prob = [.7, .4, .5, .9, .1], элементы лежать в диапазоне [0, 1], значения кратны 0.1

"""



import random
input_list=[1,3,4,13,45,67,44,56,34,32,34,33,55]
prob=[.9,.8,.2,.1,.4,.3,.1,.9,.1,.5,.6,.7,1]

def f(N,k,input_list):
    """ На задание № 1 """
    list1 = []
    index_list = []
    while len(list1) < k:
        r=random.randint(0,N-1) # получаем случайный индекс входного списка 
        if r not in index_list: # проверка полученного индекса на уникальность
            list1.append(input_list[r])
            index_list.append(r)
    print(f'N={N}, k={k}, list1={list1}') # print вместо return

     

def f1(N,k,input_list,prob):
    """ На задание № 2 """
    list1 = []
    index_list =[]
    weights=[] # список соответствующих вероятностей быть выбранными для чисел, помещаемых в list1, 
    while len(list1) < k:
        r=random.randint(0,N-1) # получаем случайный индекс входного списка
        if r not in index_list: # проверка полученного индекса на уникальность
            weights.append(prob[r]) 
            list1.append(input_list[r])
            index_list.append(r)
    result=random.choices(list1,weights=weights,k=k)
    print(f'N={N}, k={k}, list1={list1}, result={result}') # print вместо return для наглядности

          
if __name__ == "__main__":
    N=len (input_list)
    k=random.randint(1,N)
    f(N,k,input_list)
    f1(N,k,input_list,prob)
    
    
    
    
    
    




